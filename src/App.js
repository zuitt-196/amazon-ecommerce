import "./App.css";
import Footer from "./components/footer/Footer";

import Header from "./components/header/Header";
import Hero from "./components/hero/Hero";
import Product from "./components/product/Product";
import Slider from "./components/slider/Slider";
import Testmonail from "./components/testimonial/Testmonail";

import Virtual from "./components/virtual/Virtual";

function App() {
  return (
    <div className="App">
      <Header/>
      <Hero/>
      <Slider/>
      <Virtual/>
      <Product/>
      <Testmonail/>
      <Footer/>
      
    </div>
  );
}

export default App;
