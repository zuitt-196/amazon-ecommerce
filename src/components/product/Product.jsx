import React,{useState} from 'react'
import css from './Product.Style.module.css'
import Plane from '../../assets/plane.png'
import {ProductsData} from '../../data/products'
import { useAutoAnimate } from '@formkit/auto-animate/react'
const Product = () => {
    const [menuProducts, setmenuProducts] = useState(ProductsData);
    // DEFINE ANIMATION 
    const parent = useAutoAnimate()
    // DEFINE THE FILTER PRODUCT
    const filter = (type)=>{
        setmenuProducts(ProductsData.filter((product)=>{
           return product.type === type;
        }))
    }

    
  return (
    <div className={css.container}>
            <img src={Plane} alt="plane" />
            <h2>Our Feature Products</h2>
            <div className={css.products}>
                  {/* LEFT */}
                    <ul className={css.menu}>
                        <li onClick={()=> setmenuProducts(ProductsData)}>All</li>
                        <li onClick={() => filter("skin care")}>Skin Care</li>
                        <li onClick={() => filter("conditioner")}>Conditioners</li>
                        <li onClick={() => filter("foundation")}>Foundation</li>
                    </ul>
            

        {/* RIGHT */}
            <div className={css.listProducts} ref={parent}>
                    {menuProducts.map((product, i) =>(
                        <div className={css.product} key={i}>
                               <div className="left-s">
                                <div className='name-product'>
                                    <span>{product.name}</span> 
                                    <span> {product.detail}</span> 
                                    <span>{product.price}$</span>
                                    <div>Shop now</div>
                                </div>
                                <img src={product.img} alt="products"  className='img-product'/>
                            </div>
                          
                        </div>
                    ))}
                </div>
            </div>
    </div>
  )
}

export default Product
