import React,{useState} from 'react'
import css from  './HeaderStyles.module.css';
import Logo from '../../assets/logo.png'
import {BsFillCartFill} from 'react-icons/bs'
import {FaBars} from 'react-icons/fa'
const Header = () => {
  const [showMenu, setShowMenu] = useState(false);

  // DEFIFE TOOGGLE MENU
  const toggleMenu = () =>{
    setShowMenu((prev) => !prev)
     
  
  }



  return (
    <div className={css.container}>
        <div className={css.logo}>
          <img src={Logo} alt="logo" />
          <span>BERCASIO</span>
        </div>
      {/* RIGHT */}
        <div className={css.right} >
          {/* BARS ICON */}
          <div className={css.bars} onClick={toggleMenu}>
              <FaBars/>
          </div>

            <div className={css.menu}>
                <ul className={css.menuList} style={{display: showMenu ? 'flex' : "none"}}>
                  <li>Collection</li>
                  <li>Brands</li>
                  <li>News</li>
                  <li>Fililpino</li>
                </ul>
            </div>
            <input type="text" className={css.search} placeholder="Search...."/>
            <BsFillCartFill className={css.cart}/>
        </div>

    </div>
  )
}

export default Header