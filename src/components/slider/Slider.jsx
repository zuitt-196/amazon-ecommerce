import React from 'react'
import {Swiper,SwiperSlide} from 'swiper/react'
import './SliderStyle.css'
import { Pagination,Navigation } from 'swiper'
import {SliderProducts } from '../../data/products'

// IMPPORT SWIPER STYLES
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation'

const Slider = () => {
  return (
    <div className="s-container">
        <Swiper
        breakpoints={{
          640:{
            eslidesPerView:3
          },
          0:{
            slidesPerView:1
          }
        }} 
        modules={[Pagination,Navigation]}
        className="mySwiper"
        navigation={true}
        loopFillGroupWithBlank={true}
        slidesPerView={3}
        spaceBetween={40}
        // width={1000}
        slidesPerGroup={1}
        loop={true}
        
        >
                    {SliderProducts.map((product, i) =>(
                        <SwiperSlide 
                        key={i}>
                            <div className="left-s">
                                <div className='name-product'>
                                    <span>{product.name}</span> 
                                    <span> {product.detail}</span> 
                               
                                <span>{product.price}$</span>
                                        <div>Shop now</div>
                                </div>
                                <img src={product.img} alt="products"  className='img-product'/>
                            </div>
                          
                        
                         </SwiperSlide>
                    ))}
            
           
        </Swiper>
    </div>
  )
}

export default Slider
