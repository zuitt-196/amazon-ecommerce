import React from 'react'
import css from './HeroStyles.module.css'
import {BsBagFill} from 'react-icons/bs'
import {AiOutlineArrowRight} from 'react-icons/ai'
import HeroImg from '../../assets/hero.png'
import {motion}  from 'framer-motion'

const Hero = () => {
    const transition = {duration: 3, type:"spring"}
  return (
    <div className={css.container}>
        
        {/* LEFT SIDE HEERO */}
        <div className={css.h_sides}>
            <span className={css.text1}>skin protecion cream</span>

            <div className={css.text2}>
                <span>Trending collection</span>
                <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque aliquid dolore q</span>
            </div>
        </div>

        {/* MIDDLE SIDE HERO IMAGE SECTION*/}
        {/* BLUE CIRCLE */}
        <div className={css.wrapper}>
            <motion.div 
            initial={{bottom:"2rem"}}
            whileInView={{bottom:"1rem"}}
            transition={transition}
            className={css.blueCircle}></motion.div>
            {/* HERO IMAGE  */}
            <motion.img 
            transition={transition}
            initial={{bottom:"2rem"}}
            whileInView={{bottom:"-.5rem"}}
            src={HeroImg} alt="hero" width={600}/>

            <motion.div
            transition={transition}
            initial={{right:"4%"}}
            whileInView={{right:"2%"}}
            className={css.cart2}>
                <BsBagFill/>
                <div className={css.signup}>
                    <span>Best signup offers</span>
                    <div>
                    <AiOutlineArrowRight/>
                    </div>
                </div>
            </motion.div>
        </div>

        {/*RIGHT SIDE HERO SECTION   */}
        <div className={css.h_sides}>
            <div className={css.traffic}>
                <span>100k</span>
                <span>Monthy traffic</span>
            </div>
            <div className={css.customer}>
                <span>100K</span>
                <span>Happy customer</span>
            </div>
        </div>
    </div>
  )
}

export default Hero