import React from 'react'
import css from './TestimonialStyle.module.css'
import Hero from '../../assets/testimonialHero.png'
import {TestimonialsData} from '../../data/testimonials'
import {Swiper,SwiperSlide} from 'swiper/react'

console.log(TestimonialsData);
const Testmonail = () => {
  return (
    <div className={css.testimonial}>
        <div className={css.wrapper}>
            <div className={css.container}>
                <span>Top Rated</span>
                <span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore, quis!</span>
             </div>
                <img src={Hero} alt='hero' />
            <div className={css.container}> 
                <span>100k</span>
                <span>Happpy Customer with us</span>    
            </div>
        </div>

    <div className={css.reviews}>Review</div>
    {/* CARUSEL SECTION*/}
        <div className={css.carousel}>
        <Swiper
        slidesPerView={3}
        slidesPerGroup={1}
        spaceBetween={20}
        className={css.tCarousel}
        breakpoints={{
            856:{
                slidesPerView:3
            },
            640: {
                slidesPerView:2
            },
            0:{
                slidesPerView:1
            }
        }}
        >
    {
       TestimonialsData.map((test,i) =>(
            <SwiperSlide key={i}>
                <div className={css.testimonials}>
                    <img src={test.image} alt="img" />
                    <span>{test.comment}</span>
                    <div className={css.hr}></div>
                    <span>{test.name}</span>
                </div>
            </SwiperSlide>
       ))
    }   
            </Swiper>
        </div>
    </div>
  )
}

export default Testmonail
