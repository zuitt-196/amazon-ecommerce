import React from 'react'
import css from './FooterStyle.module.css';
import  Logo from '../../assets/logo.png'
import {InboxIcon, PhoneIcon, LocationMarkerIcon, LoginIcon, UsersIcon, LinkIcon} from '@heroicons/react/outline'

const Footer = () => {
  return (
    <div className={css.cFooterWrapper}>

    <hr />

      <div className={css.cFooter}>
        <div className={css.logo}>
          <img src={Logo} alt="logo" />
          <span>BERCASIO</span>
        </div>

        <div className={css.block}>
          <div className={css.detail}>
            <span>Contact Us</span>
            <span className={css.pngLine}>
              <LocationMarkerIcon className={css.icon}/>
              <span>Surigao del sur Bislig city, Mangagoy</span>
            </span>
            <span className={css.pngLine}>
              <PhoneIcon className={css.icon}/>
              <span>09102764396</span>
            </span>
            <span className={css.pngLine}>
              <InboxIcon className={css.icon}/>
              <span>bong@gmail.com</span>
            </span>
          </div>
        </div>
        

        <div className={css.block}>
          <div className={css.detail}>
            <span>Account</span>
            <span className={css.pngLine}>
              <LoginIcon className={css.icon}/>
              <span>Sign in </span>
            </span>
          </div>
        </div>

        <div className={css.block}>
          <div className={css.detail}>
            <span>Company</span>
            <span className={css.pngLine}>
              <UsersIcon className={css.icon}/>
              <span>About us</span>
            </span>
          </div>
        </div>

        <div className={css.block}>
          <div className={css.detail}>
            <span>Resources</span>
            <span className={css.pngLine}>
              <LinkIcon className={css.icon}/>
              <span>EXAMPLE </span>
            </span>
          </div>
        </div>
        
      </div>
     <div className={css.copyRight}>
        <span> Copyright @2023 by Bercasio.inc</span>
        <span> All rights reserved.</span>
     </div>
    </div> 
  )
}

export default Footer
